package com.transdigital.springbootweb.demo_tutorial_1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoTutorial1Application {

	public static void main(String[] args) {
		SpringApplication.run(DemoTutorial1Application.class, args);
	}

}
